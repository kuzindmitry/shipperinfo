//
//  MapShipperTableCell.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 24/05/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation

class MapShipperTableCell: ShipperTableCell {
    
    @IBOutlet weak var waitLabel: UILabel!
    
    var index: Int = 0
    
    override func update() {
        super.update()
        firstLetterLabel.text = "\(index)"
        guard let shipper = shipper else { return }
        let time = self.time(from: shipper.wait)
        waitLabel.text = string(from: time)
    }
    
    func time(from minutes: Int) -> (hour: Int, minute: Int) {
        let hours = Int(minutes/60)
        let min = minutes - (hours * 60)
        return (hour: hours, minute: min)
    }
    
    func string(from time: (hour: Int, minute: Int)) -> String {
        var string = ""
        if time.hour == 1 {
            string = "1 hour "
        } else {
            string = "\(time.hour) hours "
        }
        if time.minute < 10 {
            string += "0\(time.minute) min"
        } else {
            string += "\(time.minute) min"
        }
        return string
    }
    
}
