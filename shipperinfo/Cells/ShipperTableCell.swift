//
//  ShipperTableCell.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 24/05/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit

class ShipperTableCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var firstLetterLabel: UILabel!
    
    var shipper: Shipper? {
        didSet {
            update()
        }
    }
    
    func update() {
        guard let shipper = shipper else { return }
        titleLabel.text = shipper.name
        addressLabel.text = shipper.address
        if let firstLetter = shipper.name.first {
            firstLetterLabel.text = "\(firstLetter)".uppercased()
        } else {
            firstLetterLabel.text = nil
        }
    }
}
