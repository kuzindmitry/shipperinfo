//
//  ReviewTableCell.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 24/05/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation

class ReviewTableCell: UITableViewCell {
    
    @IBOutlet weak var leftItemLabel: UILabel!
    @IBOutlet weak var reviewLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var rateLabel: UILabel!
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var increaseButton: UIButton!
    
    weak var viewController: CheckInViewController?
    
    var review: Review? {
        didSet {
            updateContent()
        }
    }
    
    var activeIncreaseColor: UIColor {
        return UIColor.green
    }
    
    var activeDecreaseColor: UIColor {
        return UIColor.red
    }
    
    var inactiveColor: UIColor {
        return UIColor(red: 48/255, green: 48/255, blue: 48/255, alpha: 1)
    }
    
    func updateContent() {
        guard let review = review else { return }
        reviewLabel.text = review.comment
        rateLabel.text = "\(Int(review.comment_rating))"
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        if let date = formatter.date(from: review.created_at) {
            formatter.dateStyle = .medium
            dateLabel.text = formatter.string(from: date)
        } else {
            dateLabel.text = review.created_at
        }
        leftItemLabel.text = "\(Int(review.rating))"
        updateButtonState()
    }
    
    func updateButtonState() {
        guard let review = review else { return }
        decreaseButton.setTitleColor(review.voted < 0 ? activeDecreaseColor : inactiveColor, for: .normal)
        increaseButton.setTitleColor(review.voted > 0 ? activeIncreaseColor : inactiveColor, for: .normal)
    }
    
    @IBAction func increaseReviewRate() {
        guard var review = review, review.voted != 1 else { return }
        let vote = review.voted == -1 ? 0 : 1
        review.comment_rating += 1
        review.voted = vote
        self.review = review
        viewController?.updateReview(review)
        updateButtonState()
        API.default.voteReview(review.id, shipperId: review.shipper_id, vote: vote) { (success) in
            if !success {
                let alertController = UIAlertController(title: "Error", message: nil, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.viewController?.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func decreaseReviewRate() {
        guard var review = review, review.voted != -1 else { return }
        let vote = review.voted == 0 ? -1 : 0
        review.comment_rating -= 1
        review.voted = vote
        self.review = review
        viewController?.updateReview(review)
        updateButtonState()
        API.default.voteReview(review.id, shipperId: review.shipper_id, vote: vote) { (success) in
            if !success {
                let alertController = UIAlertController(title: "Error", message: nil, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.viewController?.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}
