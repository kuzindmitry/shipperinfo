//
//  HistoryShipperTableCell.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 24/05/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation

class HistoryShipperTableCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var firstLetterLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var secondsLabel: UILabel!
    
    var checkin: CheckIn? {
        didSet {
            update()
        }
    }
    
    func update() {
        guard let checkin = checkin else { return }
        let time = secondsToMinutesSeconds(checkin.totalSeconds)
        let hours = time.hours < 10 ? "0\(time.hours)" : "\(time.hours)"
        let minutes = time.minutes < 10 ? "0\(time.minutes)" : "\(time.minutes)"
        let seconds = time.seconds < 10 ? "0\(time.seconds)" : "\(time.seconds)"
        secondsLabel.text = hours + ":" + minutes + ":" + seconds
        if checkin.checkinDate.day == checkin.checkoutDate.day {
            let formatter = DateFormatter()
            formatter.dateFormat = "M/d/yyyy hh:mm a"
            let start = formatter.string(from: checkin.checkinDate)
            formatter.dateFormat = "hh:mm a"
            let end = formatter.string(from: checkin.checkoutDate)
            dateLabel.text = start + " - " + end
        } else {
            let formatter = DateFormatter()
            formatter.dateFormat = "M/d/yyyy hh:mm a"
            let start = formatter.string(from: checkin.checkinDate)
            let end = formatter.string(from: checkin.checkoutDate)
            dateLabel.text = start + " - " + end
        }
        titleLabel.text = checkin.shipperName
        addressLabel.text = checkin.shipperAddress
        if let firstLetter = checkin.shipperName.first {
            firstLetterLabel.text = "\(firstLetter)".uppercased()
        } else {
            firstLetterLabel.text = nil
        }
    }
    
    func secondsToMinutesSeconds(_ seconds: Int) -> (hours: Int, minutes: Int, seconds: Int) {
        return ((seconds / 3600),(seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
}
