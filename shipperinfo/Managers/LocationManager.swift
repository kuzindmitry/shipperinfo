//
//  LocationManager.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 10/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation
import MapKit

protocol LocationManagerDelegate {
    func lastLocation(_ location: CLLocation)
    func locationError()
}

class LocationManager: NSObject {
    
    let locationManager: CLLocationManager
    let delegate: LocationManagerDelegate
    
    private var isPrepared: Bool = false
    
    init(delegate: LocationManagerDelegate) {
        locationManager = CLLocationManager()
        self.delegate = delegate
        super.init()
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
    }
    
    var isFirstSignal: Bool {
        let value = UserDefaults.standard.bool(forKey: "first_signal")
        if !value {
            UserDefaults.standard.set(true, forKey: "first_signal")
        }
        return value
    }
    
    var isEnabled: Bool {
        return CLLocationManager.locationServicesEnabled() && (CLLocationManager.authorizationStatus() == .authorizedAlways || CLLocationManager.authorizationStatus() == .authorizedWhenInUse)
    }
    
    var canResume: Bool {
        return isEnabled && isPrepared
    }
    
    func start() {
        locationManager.requestAlwaysAuthorization()
    }
    
    func resume() {
        if canResume {
            start()
        } else {
            locationManager.startUpdatingLocation()
        }
    }
    
    func stop() {
        locationManager.stopUpdatingLocation()
    }
    
    deinit {
        stop()
    }
    
}

extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
            isPrepared = true
        } else {
            delegate.locationError()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }

        delegate.lastLocation(location)
        EventManager.default.currentLocation = location
        EventManager.default.setEvent()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        delegate.locationError()
    }
    
}
