//
//  NotificationManager.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 12/05/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UserNotifications

class NotificationManager: NSObject {
    
    static let `default` = NotificationManager()
    
    var handler: UIViewController?
    
    private override init() {
        super.init()
        UNUserNotificationCenter.current().delegate = self
    }
    
    func register() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            print("notifiction isOn - ", granted)
        }
    }
    
    func setTimerNotification() {
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: Double(15/*5 * 60*/), repeats: false)
        
        let notificationContent = UNMutableNotificationContent()
        notificationContent.title = "Check in in progress"
        notificationContent.body = "at " + (TimeManager.shared.currentShipperName ?? "")
        let request = UNNotificationRequest(identifier: "shipperinfo-timer-notification", content: notificationContent, trigger: trigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    }
    
    func removeNotifications() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
}

extension NotificationManager: UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, openSettingsFor notification: UNNotification?) {
        
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        
        if let shipper = TimeManager.shared.currentShipper, handler?.navigationController?.topViewController?.className != "CheckInViewController" {
            let checkInViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
            checkInViewController.shipper = shipper
            handler?.navigationController?.pushViewController(checkInViewController, animated: true)
        }
        
        completionHandler()
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
  
        completionHandler([.alert, .badge, .sound])
    }
    
}
