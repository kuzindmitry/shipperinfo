//
//  TimeManager.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 10/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation

protocol TimeManagerDelegate: class {
    func updateTime(_ trackedTime: Int, isActive: Bool)
}

class TimeManager {
    
    static let shared = TimeManager()
    weak var delegate: TimeManagerDelegate? {
        didSet {
            if delegate != nil {
                Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { _ in
                    TimeManager.shared.delegate?.updateTime(TimeManager.shared.currentTrackedTime, isActive: TimeManager.shared.isActive)
                }
            }
        }
    }
    private let defaults = UserDefaults.standard
    
    var startEventId: Int {
        return UserDefaults.standard.integer(forKey: "start_event_id")
    }
    
    private var currentTrackedTime: Int {
        var time = UserDefaults.standard.integer(forKey: "tracked_time")
        if let startDate = UserDefaults.standard.value(forKey: "start_time") as? Date, isActive {
            time += Int(Date().timeIntervalSince1970 - startDate.timeIntervalSince1970)
        }
        return time
    }
    
    var isActive: Bool {
        return UserDefaults.standard.bool(forKey: "is_active")
    }
    
    var isPaused: Bool {
        return UserDefaults.standard.bool(forKey: "is_paused")
    }
    
    var currentShipperName: String? {
        get {
            return UserDefaults.standard.string(forKey: "time_shipper_name")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "time_shipper_name")
        }
    }
    
    var currentShipper: Shipper? {
        get {
            guard let data = UserDefaults.standard.data(forKey: "time_shipper") else { return nil }
            do {
                let shipper = try JSONDecoder().decode(Shipper.self, from: data)
                return shipper
            } catch {
                print(error)
            }
            return nil
        }
        set {
            guard let newValue = newValue else {
                UserDefaults.standard.set(nil, forKey: "time_shipper")
                return
            }
            do {
                let data = try JSONEncoder().encode(newValue)
                UserDefaults.standard.set(data, forKey: "time_shipper")
            } catch {
                print(error)
            }
        }
    }
    
    func startTime() {
        UserDefaults.standard.set(true, forKey: "is_active")
        UserDefaults.standard.set(Date(), forKey: "start_time")
        UserDefaults.standard.set(0, forKey: "tracked_time")
        DispatchQueue.global().async {
            if let event = Database.shared.lastEventId() {
                UserDefaults.standard.set(event.id, forKey: "start_event_id")
            }
        }
    }
    
    func resumeTime() {
        UserDefaults.standard.set(true, forKey: "is_active")
        UserDefaults.standard.set(Date(), forKey: "start_time")
    }
    
    func stopTime(_ isPause: Bool) {
        guard let startDate = UserDefaults.standard.value(forKey: "start_time") as? Date else { return }
        var trackedTime = UserDefaults.standard.integer(forKey: "tracked_time")
        trackedTime += Int(Date().timeIntervalSince1970 - startDate.timeIntervalSince1970)
        UserDefaults.standard.set(trackedTime, forKey: "tracked_time")
        UserDefaults.standard.set(false, forKey: "is_active")
        UserDefaults.standard.set(isPause, forKey: "is_paused")
    }
    
}
