//
//  EventManager.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 20/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation
import MapKit

enum AppEventType: Int {
    case location, timerStart, timerEnd
}

class EventManager {
    
    static let `default` = EventManager()
    
    private var timer: Timer!
    private(set) var locationManager: LocationManager!
    var currentLocation: CLLocation?
    private(set) var speed: Double = 0
    
    private(set) var checkoutId: String {
        get {
            return UserDefaults.standard.string(forKey: "current_checkout_id") ?? UUID().uuidString
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "current_checkout_id")
        }
    }
    private(set) var startDate: Int {
        get {
            return UserDefaults.standard.integer(forKey: "current_start_date_event")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "current_start_date_event")
        }
    }
    private(set) var isActive: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "current_event_manager_active")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "current_event_manager_active")
        }
    }
    
    func start(_ isResume: Bool = false) {
        isActive = true
        if !isResume {
            checkoutId = UUID().uuidString
            startDate = Date().timestamp
        }
        locationManager = LocationManager(delegate: self)
        createEvent(.timerStart) {
            self.timer = Timer.scheduledTimer(withTimeInterval: 3.0, repeats: true, block: { [weak self] _ in
                self?.createEvent()
            })
        }
    }
    
    func setEvent() {
        if timer != nil && timer.isValid {
            return
        }
        createEvent()
    }
    
    private func createEvent(_ type: AppEventType = .location, _ completion: (() -> Void)? = nil) {
        guard isActive else { return }
        DispatchQueue.global().async {
            if self.speed.isNaN == true {
                self.speed = 0
            }
            
            let shipperId = TimeManager.shared.currentShipper?.id ?? ""
            
            let event = Event()
            let eventId = Database.shared.maxId(Event.self)
            event.id = Database.shared.maxId(Event.self)
            event.cid = UUID().uuidString
            event.eventType = type.rawValue
            event.createDate = Date()
            event.longitude = self.currentLocation?.coordinate.longitude ?? 0
            event.latitude = self.currentLocation?.coordinate.latitude ?? 0
            event.speed = self.speed
            event.altitude = self.currentLocation?.altitude ?? 0
            event.locationDate = self.currentLocation?.timestamp ?? Date()
            event.checkoutId = self.checkoutId
            event.bootTime = Date().timestamp - self.startDate
            event.shipperId = shipperId
            Database.shared.add(event)
            API.default.sendEvent(event, { (success) in
                if success {
                    DispatchQueue.global().async {
                        if let ev = Database.shared.event(id: eventId) {
                            let copiedEv = Event(value: ev)
                            copiedEv.deliveryDate = Date()
                            copiedEv.eventStatus = 1
                            Database.shared.add(copiedEv)
                        }
                    }
                    completion?()
                }
            })
        }
    }
    
    func end() {
        guard timer != nil else { return }
        timer.invalidate()
        timer = nil
    }
    
    func finish(_ completion: (() -> Void)? = nil) {
        end()
        createEvent(.timerEnd) {
            self.isActive = false
            self.checkoutId = UUID().uuidString
            self.startDate = 0
            completion?()
        }
    }
    
}

extension EventManager: LocationManagerDelegate {
    
    func lastLocation(_ location: CLLocation) {
        
        if let oldLocation = currentLocation {
            let distanceChange = location.distance(from: oldLocation)
            let sinceLastUpdate = location.timestamp.timeIntervalSince(oldLocation.timestamp)
            let speed = distanceChange / sinceLastUpdate
            self.speed = speed
        }
        
        currentLocation = location
    }
    
    func locationError() {
    }
    
}

extension Date {
    
    var timestamp: Int {
        return Int(timeIntervalSince1970)
    }
    
}
