//
//  AppDelegate.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 05/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        NotificationManager.default.removeNotifications()
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        Database.shared.prepare()
        if EventManager.default.isActive {
            EventManager.default.start(true)
        }
        NotificationManager.default.removeNotifications()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        EventManager.default.end()
        if TimeManager.shared.isActive {
            NotificationManager.default.setTimerNotification()
        }
    }

}
