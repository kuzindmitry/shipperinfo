//
//  AddShipperViewController.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 05/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit

class AddShipperViewController: UIViewController {
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var addressField: UITextField!
    @IBOutlet weak var cityField: UITextField!
    @IBOutlet weak var stateField: UITextField!
    
    var name: String?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        nameField.becomeFirstResponder()
        
        nameField.text = name
    }
    
    @IBAction func nextButtonDidTap() {
        guard let name = nameField.text, let address = addressField.text, let city = cityField.text, let state = stateField.text, !name.isEmpty, !address.isEmpty, !city.isEmpty, !state.isEmpty else {
            let alertController = UIAlertController(title: "Error", message: "All fields required", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            present(alertController, animated: true, completion: nil)
            return
        }
        API.default.reportShipper(name, address: address, city: city, state: state) { (shipper) in
            if let shipper = shipper {
                let viewController = self.storyboard!.instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
                viewController.shipper = shipper
                self.navigationController?.pushViewController(viewController, animated: true)
            } else {
                let alertController = UIAlertController(title: "Error", message: nil, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
}
