//
//  CheckInViewController.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 10/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit

class CheckInViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var firstLetterLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var waitTimeLabel: UILabel!
    @IBOutlet weak var vendingMachinesLabel: UILabel!
    @IBOutlet weak var lumperFeeLabel: UILabel!
    @IBOutlet weak var restroomLabel: UILabel!
    @IBOutlet weak var waitroomLabel: UILabel!
    @IBOutlet weak var overnightParkingLabel: UILabel!
    @IBOutlet weak var detentionLabel: UILabel!
    @IBOutlet weak var dockCountLabel: UILabel!
    @IBOutlet weak var timeHeightLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var reviewsEmptyLabel: UILabel!
    
    var shipper: Shipper!
    var reviews: [Review] = []
    var trackedTime: Int = 0
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        API.default.getReviews(shipper.id) { (reviews) in
            self.reviews = reviews
            self.reviewsEmptyLabel.isHidden = !reviews.isEmpty
            self.tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        reviewsEmptyLabel.isHidden = true
        revealViewController()?.panGestureRecognizer()
        title = shipper.name
        titleLabel.text = shipper.name
        addressLabel.text = shipper.address
        if let firstLetter = shipper.name.first {
            firstLetterLabel.text = "\(firstLetter)".uppercased()
        } else {
            firstLetterLabel.text = nil
        }
        
        let time = self.time(from: shipper.wait)
        waitTimeLabel.text = string(from: time)
        
        vendingMachinesLabel.text = shipper.attributes?.vendingMachines == true ? "yes" : "no"
        lumperFeeLabel.text = shipper.attributes?.lumperFee == true ? "yes" : "no"
        restroomLabel.text = shipper.attributes?.restroom == true ? "yes" : "no"
        waitroomLabel.text = shipper.attributes?.waitroom == true ? "yes" : "no"
        overnightParkingLabel.text = shipper.attributes?.overnightParking == true ? "yes" : "no"
        detentionLabel.text = shipper.attributes?.detentionPaid == true ? "yes" : "no"
        dockCountLabel.text = shipper.attributes?.dockCount ?? "-"
        
        if TimeManager.shared.isActive && TimeManager.shared.currentShipper?.id == shipper.id {
            setActiveState()
            TimeManager.shared.delegate = self
        } else {
            setDefaultState(false)
        }
     
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "SummaryShow" {
            
        }
        if segue.identifier == "CreateReview" {
            let vc = segue.destination as! ReviewViewController
            vc.shipper = shipper
        }
    }
    
    func anotherTimerIsExist() -> Bool {
        return TimeManager.shared.isActive && TimeManager.shared.currentShipper?.id != shipper.id
    }
    
    func checkCurrentTimer() {
        if anotherTimerIsExist() {
            let alertController = UIAlertController(title: "Another Check In", message: "Another Check In with '\(TimeManager.shared.currentShipper?.name ?? "")' is in progress.\nWould you like to discard that and start with '\(shipper.name)' instead?", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Yes", style: .default) { [weak self] _ in
                TimeManager.shared.stopTime(false)
                TimeManager.shared.currentShipperName = self?.shipper.name
                TimeManager.shared.currentShipper = self?.shipper
                self?.startButtonDidTap()
            }
            let noAction = UIAlertAction(title: "No", style: .default, handler: nil)
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    func updateReview(_ review: Review) {
        guard let index = reviews.firstIndex(where: { $0.id == review.id }) else { return }
        reviews.insert(review, at: index)
        reviews.remove(at: index + 1)
    }
    
    func setActiveState() {
        stopButton.isHidden = false
        startButton.isHidden = true
        TimeManager.shared.delegate = self
        TimeManager.shared.currentShipperName = shipper.name
        TimeManager.shared.currentShipper = shipper
        subtitleLabel.text = "finish load/unload and press STOP"
        timeHeightLabelConstraint.constant = 24
        view.layoutIfNeeded()
    }
    
    func setDefaultState(_ isPaused: Bool) {
        stopButton.isHidden = true
        startButton.isHidden = false
        TimeManager.shared.delegate = nil
        if !isPaused {
            timeLabel.text = nil
        }
        subtitleLabel.text = "start load/unload and press START"
        timeHeightLabelConstraint.constant = 0
        view.layoutIfNeeded()
    }
    
    @IBAction func startButtonDidTap() {
        if anotherTimerIsExist() {
            checkCurrentTimer()
            return
        }
        if TimeManager.shared.isPaused {
            TimeManager.shared.resumeTime()
        } else {
            TimeManager.shared.startTime()
        }
        setActiveState()
        EventManager.default.start()
    }
    
    @IBAction func stopButtonDidTap() {
        let endDate = Date()
        let startDate = endDate.addingTimeInterval(-Double(trackedTime))
        
        let checkIn = CheckIn()
        
        if let startEvent = Database.shared.event(id: TimeManager.shared.startEventId) {
            checkIn.checkinEventId = startEvent.id
        }
        checkIn.checkinDate = startDate
        checkIn.checkoutDate = endDate
        
        if let lastEvent = Database.shared.lastEventId() {
            checkIn.checkoutEventId = lastEvent.id
            checkIn.lastEventId = lastEvent.id
            checkIn.lastEventDate = lastEvent.createDate
            checkIn.checkoutId = lastEvent.checkoutId
        }
        
        checkIn.cid = UUID().uuidString
        checkIn.shipperId = shipper.id
        checkIn.shipperName = shipper.name
        checkIn.shipperAddress = shipper.address
        checkIn.totalSeconds = trackedTime
        Database.shared.add(checkIn)
        
        stopButton.isHidden = true
        EventManager.default.finish {
            let summaryViewController = self.storyboard!.instantiateViewController(withIdentifier: "SummaryViewController") as! SummaryViewController
            summaryViewController.checkIn = checkIn
            self.navigationController?.pushViewController(summaryViewController, animated: true)
            TimeManager.shared.stopTime(false)
            self.setDefaultState(false)
        }
    }
    
    func time(from minutes: Int) -> (hour: Int, minute: Int) {
        let hours = Int(minutes/60)
        let min = minutes - (hours * 60)
        return (hour: hours, minute: min)
    }
    
    func string(from time: (hour: Int, minute: Int)) -> String {
        var string = ""
        if time.hour == 1 {
            string = "1 hour "
        } else {
            string = "\(time.hour) hours "
        }
        if time.minute < 10 {
            string += "0\(time.minute) min"
        } else {
            string += "\(time.minute) min"
        }
        return string
    }
    
}

extension CheckInViewController: UITableViewDataSource, UITableViewDelegate {
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(ReviewTableCell.self, for: indexPath)
        let review = reviews[indexPath.row]
        cell.review = review
        return cell
    }
    
}

extension CheckInViewController: TimeManagerDelegate {
    
    func updateTime(_ trackedTime: Int, isActive: Bool) {
        let time = secondsToMinutesSeconds(trackedTime)
        let hours = time.hours < 10 ? "0\(time.hours)" : "\(time.hours)"
        let minutes = time.minutes < 10 ? "0\(time.minutes)" : "\(time.minutes)"
        let seconds = time.seconds < 10 ? "0\(time.seconds)" : "\(time.seconds)"
        timeLabel.text = hours + ":" + minutes + ":" + seconds
        self.trackedTime = trackedTime
    }
    
    func secondsToMinutesSeconds(_ seconds: Int) -> (hours: Int, minutes: Int, seconds: Int) {
        return ((seconds / 3600),(seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
}
