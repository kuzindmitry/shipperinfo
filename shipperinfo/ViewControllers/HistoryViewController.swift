//
//  HistoryViewController.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 05/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit

class HistoryViewController: UIViewController {

    private var items: [CheckIn] = []
    
    @IBOutlet private weak var tableView: UITableView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = false
        updateContent()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateContent()
    }
    
    func updateContent() {
        items = Database.shared.checkins()
        tableView.reloadData()
    }
    
    @IBAction func toggleMenu() {
        revealViewController()?.revealToggle(animated: true)
    }
    
}

extension HistoryViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(HistoryShipperTableCell.self, for: indexPath)
        cell.checkin = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let checkIn = items[indexPath.row]
        let shipper = Shipper(id: checkIn.shipperId, name: checkIn.shipperName, address: checkIn.shipperAddress, rating: 0)
        let viewController = self.storyboard!.instantiateViewController(withIdentifier: "CheckInViewController") as! CheckInViewController
        viewController.shipper = shipper
        navigationController?.pushViewController(viewController, animated: true)
    }
    
}
