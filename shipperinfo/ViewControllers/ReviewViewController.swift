//
//  ReviewViewController.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 10/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit
import Cosmos

class ReviewViewController: UIViewController {
    
    @IBOutlet weak var cosmosView: CosmosView!
    @IBOutlet weak var shipperLabel: UILabel!
    @IBOutlet weak var reviewTextView: OTPlaceholderTextView!
    
    var shipper: Shipper!
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        reviewTextView.becomeFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shipperLabel.text = "Your review about '\(shipper.name)'"
    }
    
    @IBAction func nextTimeDidTap() {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func sendReview() {
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "AdditionalReview" {
            let vc = segue.destination as! AdditionalReviewViewController
            vc.comment = reviewTextView.text
            vc.rate = cosmosView.rating
            vc.shipper = shipper
        }
    }
    
}

class AdditionalReviewViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var comment: String = ""
    var rate: Double = 0
    var shipper: Shipper!
    
    @IBOutlet weak var tableView: UITableView!
    
    var timeIndexPath: IndexPath?
    var appointmentIndexPath: IndexPath?
    
    let times: [String] = ["1 hour", "2 hours", "3 hours", "4 hours", "5 hours", "6 hours", "more than 6 hours"]
    let appointments: [String] = ["before", "on time", "after", "no appointment"]
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Wait Time"
        }
        return "Appointment"
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return section == 0 ? times.count : appointments.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AdditionalInfoReviewTableCell", for: indexPath)
        cell.textLabel?.text = indexPath.section == 0 ? times[indexPath.row] : appointments[indexPath.row]
        cell.tintColor = .white
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if indexPath.section == 0 {
            if let ip = timeIndexPath {
                let ipCell = tableView.cellForRow(at: ip)
                ipCell?.accessoryType = .none
            }
            if indexPath.row == timeIndexPath?.row {
                timeIndexPath = nil
            } else {
                cell?.accessoryType = .checkmark
                timeIndexPath = indexPath
            }
        } else if indexPath.section == 1 {
            if let ip = appointmentIndexPath {
                let ipCell = tableView.cellForRow(at: ip)
                ipCell?.accessoryType = .none
            }
            if indexPath.row == appointmentIndexPath?.row {
                appointmentIndexPath = nil
            } else {
                cell?.accessoryType = .checkmark
                appointmentIndexPath = indexPath
            }
        }
    }
    
    @IBAction func sendReview() {
        guard let viewControllers = navigationController?.viewControllers else { return }
        guard viewControllers.count > 2 else { return }
        print(viewControllers)
        
        API.default.sendReview(shipper.id, text: comment, rating: rate, time: timeIndexPath?.row ?? 0 + 1) { [unowned self] (success) in
            if success {
                let controller = viewControllers[viewControllers.count - 3]
                self.navigationController?.popToViewController(controller, animated: true)
            } else {
                let alertController = UIAlertController(title: "Error", message: nil, preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    @IBAction func nextTimeDidTap() {
        guard let viewControllers = navigationController?.viewControllers else { return }
        guard viewControllers.count > 2 else { return }
        let controller = viewControllers[viewControllers.count - 3]
        navigationController?.popToViewController(controller, animated: true)
    }
    
}
