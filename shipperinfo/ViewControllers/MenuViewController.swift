//
//  ViewController.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 05/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit
import SafariServices

class MenuViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    let items: [String] = ["Map", "Checkin History", "Terms of use"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuItemCell", for: indexPath)
        cell.textLabel?.text = items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            performSegue(withIdentifier: "showMap", sender: nil)
        } else if indexPath.row == 1 {
            performSegue(withIdentifier: "showHistory", sender: nil)
        } else {
            let safariViewController = SFSafariViewController(url: URL(string: "https://shipperinfo.com/site/terms")!)
            present(safariViewController, animated: true, completion: nil)
            revealViewController()?.revealToggle(animated: false)
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
