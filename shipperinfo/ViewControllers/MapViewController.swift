//
//  MapViewController.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 05/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var resultsHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var retryView: UIView!
    @IBOutlet weak var menuRightConstraint: NSLayoutConstraint!
    
    private var locationManager: LocationManager!
    var items: [Shipper] = []
    var searchedItems: [Shipper] = []
    var annotations: [MKAnnotation] = []
    var selectedShipper: Shipper!
    var isSearchActive: Bool = false
    var isAutoRetry: Bool = true
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.isNavigationBarHidden = true
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        retryView.isHidden = false
        self.resultsHeightConstraint.constant = UIScreen.main.bounds.height/2
        revealViewController()?.panGestureRecognizer()
        UITextField.appearance(whenContainedInInstancesOf: [UISearchBar.self]).defaultTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        tableView.keyboardDismissMode = .onDrag
        locationManager = LocationManager(delegate: self)
        
        NotificationManager.default.register()
        NotificationManager.default.handler = self
        
        menuRightConstraint.constant = UIScreen.main.bounds.width == 375.0 ? 5 : -8
        view.layoutIfNeeded()
        
        addObservers()
    }
    
    func addObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidShow(_:)), name: UIApplication.keyboardDidShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(_:)), name: UIApplication.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive(_:)), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willResignActive(_:)), name: UIApplication.willResignActiveNotification, object: nil)
    }
    
    
    @IBAction func changeAutoRetry(_ switchControl: UISwitch) {
        isAutoRetry = switchControl.isOn
    }
    
    @objc func willResignActive(_ notification: Notification) {
        locationManager.stop()
    }
    
    @objc func didBecomeActive(_ notification: Notification) {
        locationManager.resume()
        if TimeManager.shared.isActive && self.navigationController?.topViewController == self {
            let alertController = UIAlertController(title: "Active Check In", message: "Active Check In with '\(TimeManager.shared.currentShipperName ?? "")' is in progress.\nWould you like to continue?", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Yes", style: .default) { [weak self] _ in
                self?.selectedShipper = TimeManager.shared.currentShipper
                self?.performSegue(withIdentifier: "ShowCheckIn", sender: self)
            }
            let noAction = UIAlertAction(title: "No", style: .default) { _ in
                TimeManager.shared.stopTime(false)
            }
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            present(alertController, animated: true, completion: nil)
        }
    }
    
    var topViewController: UIViewController? {
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            if topController is SWRevealViewController {
                topController = (topController as! SWRevealViewController).frontViewController
            }
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            return topController
        }
        return nil
    }
    
    @objc func keyboardDidShow(_ notification: Notification) {
        bottomConstraint.constant = -80 + 10
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func toggleMenu() {
        revealViewController()?.revealToggle(animated: true)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        bottomConstraint.constant = 10
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    var isUpdating: Bool = true
    func update(_ coordinate: CLLocationCoordinate2D, updateMap: Bool) {
        guard !isUpdating else { return }
        isUpdating = true
        API.default.mapSearchAround(coordinate) { (shippers) in
            self.items = shippers
            self.annotations = []
            self.tableView.reloadData()
            self.mapView.removeAnnotations(self.mapView.annotations)
            shippers.forEach {
                self.addMarker($0)
            }
            self.isUpdating = false
            self.retryView.isHidden = true
            
            if updateMap {
                self.mapView.showAnnotations(self.annotations, animated: true)
            }
            
            if (shippers.isEmpty && self.isAutoRetry) {
                self.update(coordinate, updateMap: updateMap)
            }
        }
    }
    
    func startTimer(for shipper: Shipper) {
        selectedShipper = shipper
        performSegue(withIdentifier: "ShowCheckIn", sender: self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        locationManager.resume()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        locationManager.stop()
    }
    
    
    
    func addMarker(_ shipper: Shipper) {
        guard let location = shipper.location else { return }
        let coordinate = CLLocationCoordinate2D(latitude: location.lat, longitude: location.lon)
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        annotations.append(annotation)
        mapView.addAnnotation(annotation)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ShowCheckIn" {
            let vc = segue.destination as! CheckInViewController
            vc.shipper = selectedShipper
        }
    }
    var isFirstAppear: Bool = true
}

extension MapViewController: LocationManagerDelegate {
    
    func locationError() {
        print("Location error")
    }
    
    func lastLocation(_ location: CLLocation) {
        let region = MKCoordinateRegion(center: location.coordinate, latitudinalMeters: 1000, longitudinalMeters: 1000)
        print(location)
        if isFirstAppear {
            mapView.setRegion(region, animated: false)
        }
        isUpdating = false
        if isFirstAppear {
            update(location.coordinate, updateMap: true)
        }
        isFirstAppear = false
    }
    
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }
        guard let index = annotations.firstIndex(where: { $0.hash == annotation.hash }) else { return }
        if isSearchActive && searchedItems.count > index {
            startTimer(for: searchedItems[index])
        } else if items.count > index {
            startTimer(for: items[index])
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            let pin = mapView.view(for: annotation) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
            let image = UIImage(named: "ic_map_truck")
            pin.image = resizeImage(image: image!, targetSize: CGSize(width: 30, height: 30))
            pin.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
            pin.layer.shadowRadius = 6
            pin.layer.shadowOffset = .zero
            pin.layer.shadowOpacity = 1
            return pin
        } else if let index = annotations.firstIndex(where: { $0.hash == annotation.hash }) {
            let pin = mapView.view(for: annotation) ?? MKAnnotationView(annotation: annotation, reuseIdentifier: nil)
            let image = UIImage(named: "ic_map_shipper")
            pin.annotation = annotation
            pin.image = resizeImage(image: image!, targetSize: CGSize(width: 30, height: 30))
            pin.layer.shadowColor = UIColor.black.withAlphaComponent(0.5).cgColor
            pin.layer.shadowRadius = 6
            pin.layer.shadowOffset = .zero
            pin.layer.shadowOpacity = 1
            
            let label = UILabel(frame: CGRect(x: 0, y: 0, width: 24, height: 20))
            label.textAlignment = .center
            label.textColor = .white
            label.font = UIFont.systemFont(ofSize: 13)
            label.text = "\(index + 1)"
            pin.addSubview(label)
            
            return pin
        }
        return nil
    }
    
    func resizeImage(image: UIImage, targetSize: CGSize) -> UIImage {
        let size = image.size
        
        let widthRatio  = targetSize.width  / size.width
        let heightRatio = targetSize.height / size.height
        
        var newSize: CGSize
        if(widthRatio > heightRatio) {
            newSize = CGSize(width: size.width * heightRatio, height: size.height * heightRatio)
        } else {
            newSize = CGSize(width: size.width * widthRatio,  height: size.height * widthRatio)
        }
        
        let rect = CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 1.0)
        image.draw(in: rect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
}

extension MapViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isSearchActive ? searchedItems.count : items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(MapShipperTableCell.self, for: indexPath)
        cell.index = indexPath.row + 1
        cell.shipper = isSearchActive ? searchedItems[indexPath.row] : items[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        startTimer(for: isSearchActive ? searchedItems[indexPath.row] : items[indexPath.row])
    }
    
}

extension MapViewController: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        search(searchText)
    }

    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        isSearchActive = true
        tableView.reloadData()
        annotations = []
        mapView.removeAnnotations(mapView.annotations)
        searchBar.showsCancelButton = true
        return true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        search(searchBar.text) {
            if self.searchedItems.isEmpty {
                let alertController = UIAlertController(title: "We do not have this Shipper in our DB", message: "Do you want to report a new Shipper?", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
                alertController.addAction(UIAlertAction(title: "Yes", style: .default, handler: { [unowned self] _ in
                    let controller = self.storyboard!.instantiateViewController(withIdentifier: "AddShipperViewController") as! AddShipperViewController
                    controller.name = searchBar.text
                    self.navigationController?.pushViewController(controller, animated: true)
                }))
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.text = nil
        isSearchActive = false
        tableView.reloadData()
        annotations = []
        mapView.removeAnnotations(mapView.annotations)
        items.forEach {
            self.addMarker($0)
        }
        mapView.showAnnotations(annotations, animated: true)
        searchBar.showsCancelButton = false
    }
    
    func search(_ text: String?, _ completion: (() -> Void)? = nil) {
        guard let text = text, !text.isEmpty else { return }
        API.default.mapSearch(text) { (items) in
            self.searchedItems = items
            self.mapView.removeAnnotations(self.annotations)
            self.annotations = []
            items.forEach {
                self.addMarker($0)
            }
            self.mapView.showAnnotations(self.annotations, animated: true)
            completion?()
            self.tableView.reloadData()
        }
    }
    
}
