//
//  SummaryViewController.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 10/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit

class SummaryViewController: UIViewController {
    
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var firstLetterLabel: UILabel!
    @IBOutlet weak var mapImageView: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var checkIn: CheckIn?
    
    lazy var dateFormatter: DateFormatter = {
        let df = DateFormatter()
        df.dateFormat = "M/d/yyyy hh:mm a"
        return df
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let date = checkIn?.checkinDate {
            startLabel.text = dateFormatter.string(from: date)
        }
        if let date = checkIn?.checkoutDate {
            endLabel.text = dateFormatter.string(from: date)
        }
        let time = secondsToMinutesSeconds(checkIn?.totalSeconds ?? 0)
        let hours = time.hours < 10 ? "0\(time.hours)" : "\(time.hours)"
        let minutes = time.minutes < 10 ? "0\(time.minutes)" : "\(time.minutes)"
        let seconds = time.seconds < 10 ? "0\(time.seconds)" : "\(time.seconds)"
        timeLabel.text = hours + ":" + minutes + ":" + seconds
        
        titleLabel.text = checkIn?.shipperName
        addressLabel.text = checkIn?.shipperAddress
        if let firstLetter = checkIn?.shipperName.first {
            firstLetterLabel.text = "\(firstLetter)".uppercased()
        } else {
            firstLetterLabel.text = nil
        }
        
        if let checkoutId = checkIn?.checkoutId, let shipperId = checkIn?.shipperId, !checkoutId.isEmpty, !shipperId.isEmpty {
            setMap(for: checkoutId, with: shipperId)
        }
    }
    
    func setMap(for checkoutId: String, with shipperId: String) {
        let request = URLRequest(url: URL(string: "https://api.shipperinfo.com/ios/shipper/\(shipperId)/checkout/\(checkoutId)/map")!)
        activityIndicator.startAnimating()
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                print(httpResponse.statusCode)
                if httpResponse.statusCode != 200 {
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                        self.setMap(for: checkoutId, with: shipperId)
                    })
                    return
                }
            }
            guard let data = data else { return }
            print(data)
            let image = UIImage(data: data)
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.mapImageView.image = image
            }
        }.resume()
    }
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    func showAlert(_ title: String?, _ message: String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func sendEmail() {
        let alertController = UIAlertController(title: "Received by email", message: nil, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "Email"
        }
        let action = UIAlertAction(title: "Send", style: .default) { [weak self] _ in
            guard let email = alertController.textFields![0].text, !email.isEmpty, self?.isValidEmail(testStr: email) == true, let shipperId = self?.checkIn?.shipperId, let checkoutId = self?.checkIn?.checkoutId else {
                self?.showAlert("Error", "Not valid email")
                return
            }
            API.default.reportEmail(shipperId, email: email, checkoutId: checkoutId, { (success) in
                if success {
                    self?.showAlert("Success", nil)
                } else {
                    self?.showAlert("Error", nil)
                }
            })
        }
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(action)
        alertController.addAction(cancel)
        present(alertController, animated: true, completion: nil)
    }
    
    func secondsToMinutesSeconds(_ seconds: Int) -> (hours: Int, minutes: Int, seconds: Int) {
        return ((seconds / 3600),(seconds % 3600) / 60, (seconds % 3600) % 60)
    }
    
    @IBAction func doneButtonDidTap() {
        navigationController?.popToRootViewController(animated: true)
    }
    
}
