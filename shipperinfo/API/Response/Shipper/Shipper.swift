//
//  Shipper.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 06/08/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation

struct Shipper: Codable {
    let id: String
    let name: String
    let phone: String?
    let address: String
    let rating: Double
    let wait: Int
    let location: ShipperLocation?
    let attributes: ShipperAttributes?
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case phone = "phone"
        case address = "address"
        case rating = "rating"
        case location = "location"
        case attributes = "attributes"
        case wait = "wait_time"
    }
    
    init(id: String, name: String, address: String, rating: Double) {
        self.id = id
        self.name = name
        self.address = address
        self.rating = rating
        self.location = nil
        self.phone = nil
        self.wait = 0
        self.attributes = nil
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        address = (try? container.decode(String.self, forKey: .address)) ?? ""
        rating = (try? container.decode(Double.self, forKey: .rating)) ?? 0
        location = try? container.decode(ShipperLocation.self, forKey: .location)
        phone = try? container.decode(String.self, forKey: .phone)
        wait = (try? container.decode(Int.self, forKey: .wait)) ?? 0
        attributes = try? container.decode(ShipperAttributes.self, forKey: .attributes)
    }
    
}
