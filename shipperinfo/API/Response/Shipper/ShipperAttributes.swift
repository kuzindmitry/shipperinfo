//
//  ShipperAttributes.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 06/08/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation

struct ShipperAttributes: Codable {
    let vendingMachines: Bool
    let lumperFee: Bool
    let restroom: Bool
    let waitroom: Bool
    let overnightParking: Bool
    let detentionPaid: Bool
    let dockCount: String?
    
    enum CodingKeys: String, CodingKey {
        case vendingMachines = "Vending machines"
        case lumperFee = "Lumper fee"
        case restroom = "Restroom"
        case waitroom = "Wait room"
        case overnightParking = "Overnight parking"
        case detentionPaid = "Detention paid"
        case dockCount = "Dock count"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        vendingMachines = (try? container.decode(Bool.self, forKey: .vendingMachines)) ?? false
        lumperFee = (try? container.decode(Bool.self, forKey: .lumperFee)) ?? false
        restroom = (try? container.decode(Bool.self, forKey: .restroom)) ?? false
        waitroom = (try? container.decode(Bool.self, forKey: .waitroom)) ?? false
        overnightParking = (try? container.decode(Bool.self, forKey: .overnightParking)) ?? false
        detentionPaid = (try? container.decode(Bool.self, forKey: .detentionPaid)) ?? false
        dockCount = try? container.decode(String.self, forKey: .dockCount)
    }
}
