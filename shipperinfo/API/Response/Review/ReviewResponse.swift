//
//  ReviewResponse.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 06/08/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation

struct ReviewResponse: Decodable {
    let success: Bool
    let result: [Review]
}
