//
//  Review.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 06/08/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation

struct Review: Decodable {
    let id: String
    let rating: Double
    let comment: String
    var comment_rating: Double
    let created_at: String
    let shipper_id: String
    var voted: Int
    let wait_time: Int
    
    enum CodingKeys: String, CodingKey {
        case id = "id"
        case rating = "rating"
        case comment = "comment"
        case commentRating = "comment_rating"
        case createdAt = "created_at"
        case shipperId = "shipper_id"
        case voted = "voted"
        case waitTime = "wait_time"
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        rating = try container.decode(Double.self, forKey: .rating)
        comment = try container.decode(String.self, forKey: .comment)
        comment_rating = try container.decode(Double.self, forKey: .commentRating)
        created_at = try container.decode(String.self, forKey: .createdAt)
        shipper_id = try container.decode(String.self, forKey: .shipperId)
        voted = (try? container.decode(Int.self, forKey: .voted)) ?? 0
        wait_time = (try? container.decode(Int.self, forKey: .waitTime)) ?? 0
    }
}
