//
//  API.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 13/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation
import Alamofire
import MapKit

class API {
    
    static let `default` = API()
    let baseUrl = "https://api.shipperinfo.com/ios/"
    
    enum ContentTypeApplicationType: String {
        case form = "application/x-www-form-urlencoded"
        case json = "application/json"
    }
    
    func headers(_ contentType: ContentTypeApplicationType) -> [String : String] {
        return ["Content-Type": contentType.rawValue, "X-I": UIDevice.current.identifierForVendor?.uuidString ?? "", "Authorization": "Bearer testkey"]
    }
    
    private func request(_ urlString: String,
                         method: HTTPMethod = .get,
                         parameters: [String: Any] = [:],
                         encoding: ParameterEncoding = URLEncoding.default,
                         headers: HTTPHeaders? = nil,
                         _ completion: @escaping (Data?, HTTPURLResponse?) -> Void) {
        
        var headers = headers
        if headers == nil {
            headers = self.headers(.form)
        }
        
        Alamofire.request(urlString, method: method, parameters: parameters, encoding: encoding, headers: headers).responseData { (response) in
            completion(response.data, response.response)
        }
    }
    
    func data(_ urlString: String,
              method: HTTPMethod = .get,
              parameters: [String: Any] = [:],
              encoding: ParameterEncoding = URLEncoding.default,
              headers: HTTPHeaders? = nil,
              _ completion: @escaping (Data?, HTTPURLResponse?) -> Void) {
        
        request(urlString, method: method, parameters: parameters, encoding: encoding, headers: headers, completion)
    }
    
    func json<T: Decodable>(_ urlString: String,
                            method: HTTPMethod = .get,
                            parameters: [String: Any] = [:],
                            encoding: ParameterEncoding = URLEncoding.default,
                            headers: HTTPHeaders? = nil,
                            _ completion: @escaping (T?) -> Void) {
        
        request(urlString, method: method, parameters: parameters, encoding: encoding, headers: headers) { (data, response) in
            guard let data = data else {
                completion(nil)
                return
            }
            DispatchQueue.global().async {
                do {
                    let response = try JSONDecoder().decode(T.self, from: data)
                    DispatchQueue.main.async {
                        completion(response)
                    }
                } catch {
                    DispatchQueue.main.async {
                        completion(nil)
                    }
                }
            }
        }
    }
    
    func isSuccessCode(from response: HTTPURLResponse?) -> Bool {
        guard let code = response?.statusCode else { return false }
        return code >= 200 && code < 300
    }
    
}


// MARK: Find

extension API {
    
    func find(_ q: String, _ completion: @escaping ([Shipper]) -> Void) {
        json(baseUrl.findPath, parameters: ["q": q], headers: headers(.form)) { (response: FindResponse?) in
            completion(response?.result ?? [])
        }
    }
    
}


// MARK: Map

extension API {
    
    func mapSearchAround(_ coordinate: CLLocationCoordinate2D, _ completion: @escaping ([Shipper]) -> Void) {
        json(baseUrl.suggestAroundPath, parameters: ["p": "\(coordinate.latitude),\(coordinate.longitude)", "l": "100"]) { (response: FindResponse?) in
            completion(response?.result ?? [])
        }
    }
    
    func mapSearch(_ q: String, _ completion: @escaping ([Shipper]) -> Void) {
        json(baseUrl.suggestPath, parameters: ["q": q]) { (response: FindResponse?) in
            completion(response?.result ?? [])
        }
    }
    
}



// MARK: Reviews

extension API {
    
    func getReviews(_ id: String, _ completion: @escaping ([Review]) -> Void) {
        json(baseUrl.reviewsPath(with: id)) { (response: ReviewResponse?) in
            completion(response?.result ?? [])
        }
    }
    
    func voteReview(_ reviewId: String, shipperId: String, vote: Int, _ completion: @escaping (Bool) -> Void) {
        let uuid = UUID().uuidString
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        let date = formatter.string(from: Date())
        let params = [
            "cid": "\(uuid)",
            "created_at": date,
            "review_id": "\(reviewId)",
            "shipper_id": "\(shipperId)",
            "vote": vote
        ] as [String : Any]
        
        data(baseUrl.reviewsPath(with: shipperId).voteReviewsPath(with: reviewId), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers(.json)) { (_, response) in
            completion(self.isSuccessCode(from: response))
        }
    }
    
    func sendReview(_ id: String, text: String, rating: Double, time: Int, _ completion: @escaping (Bool) -> Void) {
        
        let uuid = UUID().uuidString
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        let date = formatter.string(from: Date())
        let params = [
            "apt_form": 1,
            "cid": "\(uuid)",
            "created_at": date,
            "rating": Int(rating),
            "shipper_id": "\(id)",
            "submitted_at": date,
            "wait_time": time,
            "review": text
        ] as [String : Any]
        
        data(baseUrl.reviewsPath(with: id), method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers(.json)) { (_, response) in
            completion(self.isSuccessCode(from: response))
        }
    }
    
}


// MARK: Shipper

extension API {
    
    func reportShipper(_ name: String, address: String, city: String, state: String, _ completion: @escaping (Shipper?) -> Void) {
        let uuid = UUID().uuidString
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        let date = formatter.string(from: Date())
        
        let params = [
            "cid": "\(uuid)",
            "created_at": date,
            "address": address,
            "city": city,
            "lat": 0,
            "lon": 0,
            "name": name,
            "state": state,
            "submitted_at": date
            ] as [String : Any]
        
        json(baseUrl.reportPath, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers(.json)) { (response: ShipperResponse?) in
            completion(response?.result)
        }
    }
    
}



// MARK: Event

extension API {
    
    func sendEvent(_ event: Event, _ completion: @escaping (Bool) -> Void) {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZ"
        let createdDate = formatter.string(from: event.createDate)
        let locationDate = formatter.string(from: event.locationDate)
        let params = [
            "cid": event.cid,
            "created_date": createdDate,
            "event_type": event.eventType,
            "parcel": [
                "form": [
                    "shipper_id": event.shipperId,
                    "checkout_id": event.checkoutId
                ],
                "boot_time": event.bootTime,
                "location": [
                    "accuracy": 0,
                    "altitude": event.altitude,
                    "course": 0,
                    "latitude": event.latitude,
                    "longitude": event.longitude,
                    "location_date": locationDate,
                    "speed": event.speed
                ]
            ],
            "parcel_version": 0
            ] as [String : Any]
        
        data(baseUrl.eventPath, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers(.json)) { (_, response) in
            completion(self.isSuccessCode(from: response))
        }
    }
    
}


//MARK: Other

extension API {
    
    func reportEmail(_ shipperId: String, email: String, checkoutId: String, _ completion: @escaping (Bool) -> Void) {
        let uuid = UUID().uuidString
        let params = [
            "cid": "\(uuid)",
            "shipper_id": shipperId,
            "email": email,
            "checkout_id": checkoutId
            ] as [String : Any]
        
        data(baseUrl.reportEmailPath, method: .post, parameters: params, encoding: JSONEncoding.default, headers: headers(.json)) { (_, response) in
            completion(self.isSuccessCode(from: response))
        }
    }
    
}


// MARK: Path

extension String {
    
    var findPath: String {
        return self + "find"
    }
    
    var suggestAroundPath: String {
        return self + "suggest/around"
    }
    
    var suggestPath: String {
        return self + "suggest"
    }
    
    func reviewsPath(with id: String) -> String {
        return self + "reviews/\(id)"
    }
    
    func voteReviewsPath(with id: String) -> String {
        return self + "/\(id)/vote"
    }
    
    var reportPath: String {
        return self + "report"
    }
    
    var eventPath: String {
        return self + "event"
    }
    
    var reportEmailPath: String {
        return self + "checkout/report-email"
    }
    
}
