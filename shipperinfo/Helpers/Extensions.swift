//
//  Extensions.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 10/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit

extension UIViewController {
    
    var className: String {
        return String(describing: type(of: self))
    }
    
}

extension Date {
    var day: Int {
        return Calendar.current.component(.day, from: self)
    }
}

extension NSObject {
    
    static var className: String {
        return String(describing: self)
    }
    
}

extension UITableView {
    
    func dequeueReusableCell<Cell: UITableViewCell>(_ T: Cell.Type, for indexPath: IndexPath) -> Cell {
        guard let cell = dequeueReusableCell(withIdentifier: T.className, for: indexPath) as? Cell else {
            fatalError("Could not dequeue cell with identifier: " + String(T.className) + " or edit custom class to XIB file")
        }
        return cell
    }
    
}
