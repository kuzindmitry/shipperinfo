//
//  OTPlaceholderTextView.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 10/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import UIKit

protocol OTPlaceholderTextViewDelegate: class {
    func otPlaceholderTextView(_ textView: OTPlaceholderTextView, contentSizeDidChange contentSize: CGSize)
}

class OTPlaceholderTextView: UITextView {
    
    private let placeholderLabel: UILabel = UILabel()
    private var previousLineRect: CGRect = .zero
    private lazy var placeholderLabelConstraints = [NSLayoutConstraint]()
    weak var placeholderTextViewDelegate: OTPlaceholderTextViewDelegate?
    
    @IBInspectable var placeholder: String = "" {
        didSet {
            placeholderLabel.text = placeholder
        }
    }
    
    @IBInspectable var placeholderColor: UIColor = .lightGray {
        didSet {
            placeholderLabel.textColor = placeholderColor
        }
    }
    
    override var font: UIFont! {
        didSet {
            if placeholderFont == nil {
                placeholderLabel.font = font
            }
        }
    }
    
    var placeholderFont: UIFont? {
        didSet {
            let font = (placeholderFont != nil) ? placeholderFont : self.font
            placeholderLabel.font = font
        }
    }
    
    override var textAlignment: NSTextAlignment {
        didSet {
            placeholderLabel.textAlignment = textAlignment
        }
    }
    
    override var text: String! {
        didSet {
            textDidChange()
        }
    }
    
    override var attributedText: NSAttributedString! {
        didSet {
            textDidChange()
        }
    }
    
    override var textContainerInset: UIEdgeInsets {
        didSet {
            updateConstraintsForPlaceholderLabel()
        }
    }
    
    var placeholderLineCount: Int {
        get {
            return placeholderLabel.numberOfLines
        }
        set {
            placeholderLabel.numberOfLines = newValue
        }
    }
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    private func commonInit() {
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange), name: UITextView.textDidChangeNotification, object: nil)
        
        placeholderLabel.font = font
        placeholderLabel.textColor = placeholderColor
        placeholderLabel.textAlignment = textAlignment
        placeholderLabel.text = placeholder
        placeholderLabel.numberOfLines = 0
        placeholderLabel.backgroundColor = .clear
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(placeholderLabel)
        updateConstraintsForPlaceholderLabel()
    }
    
    private func updateConstraintsForPlaceholderLabel() {
        let newConstraints = [
            placeholderLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: textContainerInset.left + textContainer.lineFragmentPadding),
            placeholderLabel.topAnchor.constraint(equalTo: topAnchor, constant: textContainerInset.top),
            placeholderLabel.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1, constant: -(textContainerInset.left + textContainerInset.right + textContainer.lineFragmentPadding * 2.0))
        ]
        
        removeConstraints(placeholderLabelConstraints)
        addConstraints(newConstraints)
        placeholderLabelConstraints = newConstraints
    }
    
    @objc private func textDidChange() {
        placeholderLabel.isHidden = !text.isEmpty
        
        let position = endOfDocument
        let rect = caretRect(for: position)
        
        if rect.origin.y != previousLineRect.origin.y && previousLineRect != .zero {
            placeholderTextViewDelegate?.otPlaceholderTextView(self, contentSizeDidChange: contentSize)
        }
        previousLineRect = rect
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        placeholderLabel.preferredMaxLayoutWidth = textContainer.size.width - textContainer.lineFragmentPadding * 2.0
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
