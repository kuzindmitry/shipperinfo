//
//  CoreDataManager.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 05/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import Foundation
import CoreData

import RealmSwift

class Database {
    
    static let shared = Database()
    
    typealias Success = (() -> Void)
    typealias Failure = ((_ error: Error?) -> Void)
    
    private let defaultRealm: Realm
    func realmInstance() throws -> Realm {
        return Thread.isMainThread ? defaultRealm : try Realm()
    }
    
    func prepare() {}
    
    private init() {
        let configuration = Realm.Configuration(schemaVersion: 3, migrationBlock: { (migration, oldSchemaVersion) in
            if oldSchemaVersion < 2 {
                migration.enumerateObjects(ofType: Event.className(), { (oldObject, newObject) in
                    newObject!["checkoutId"] = UUID().uuidString
                    newObject!["shipperId"] = ""
                    newObject!["bootTime"] = 0
                })
            }
            if oldSchemaVersion < 3 {
                migration.enumerateObjects(ofType: CheckIn.className(), { (oldObject, newObject) in
                    newObject!["checkoutId"] = ""
                })
            }
        })
        Realm.Configuration.defaultConfiguration = configuration
        defaultRealm = try! Realm()
    }
    
    func maxId(_ type: Object.Type) -> Int {
        let realm = try? realmInstance()
        return realm?.objects(type).max(ofProperty: "id") ?? 0
    }
    
    func add(_ object: Object) {
        let realm = try? realmInstance()
        ((try? realm?.safeWrite {
            realm?.add(object, update: true)
        }) as ()??)
    }
    
    func event(id: Int) -> Event? {
        let realm = try? realmInstance()
        return realm?.object(ofType: Event.self, forPrimaryKey: id)
    }
    
    func lastEventId() -> Event? {
        let realm = try? realmInstance()
        if let id: Int = realm?.objects(Event.self).max(ofProperty: "id") {
            return event(id: id)
        }
        return nil
    }
    
    func checkins(for text: String? = nil) -> [CheckIn] {
        do {
            let realm = try realmInstance()
            if let text = text {
                return Array(realm.objects(CheckIn.self).filter(NSPredicate(format: "(shipper_name CONTAINS[cd] %@) OR (shipper_address CONTAINS[cd] %@)", text, text)).sorted(byKeyPath: "id", ascending: false))
            } else {
                return Array(realm.objects(CheckIn.self).sorted(byKeyPath: "checkinDate", ascending: false))
            }
        } catch {
            print(error)
            return []
        }
    }
    
}

extension Realm {
    
    func safeWrite(_ block: () throws -> Void) throws {
        if isInWriteTransaction {
            try block()
        } else {
            try write(block)
        }
    }
    
}
