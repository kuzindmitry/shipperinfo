//
//  Event.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 20/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import RealmSwift

class Event: Object {
    
    @objc dynamic var id: Int = 0
    @objc dynamic var cid: String = ""
    @objc dynamic var createDate: Date = Date()
    @objc dynamic var deliveryDate: Date = Date()
    @objc dynamic var eventStatus: Int = 0
    @objc dynamic var eventType: Int = 0
    @objc dynamic var parcel: String = ""
    @objc dynamic var parcelVersion: Int = 0
    @objc dynamic var latitude: Double = 0
    @objc dynamic var longitude: Double = 0
    @objc dynamic var speed: Double = 0
    @objc dynamic var altitude: Double = 0
    @objc dynamic var locationDate: Date = Date()
    @objc dynamic var checkoutId: String = UUID().uuidString
    @objc dynamic var bootTime: Int = 0
    @objc dynamic var shipperId: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
