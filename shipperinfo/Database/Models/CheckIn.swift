//
//  CheckIn.swift
//  shipperinfo
//
//  Created by Dmitry Kuzin on 20/03/2019.
//  Copyright © 2019 Shipperinfo. All rights reserved.
//

import RealmSwift

class CheckIn: Object {
    
    @objc dynamic var id: String = UUID().uuidString
    @objc dynamic var checkinDate: Date = Date()
    @objc dynamic var checkinEventId: Int = 0
    @objc dynamic var checkoutDate: Date = Date()
    @objc dynamic var checkoutEventId: Int = 0
    @objc dynamic var cid: String = ""
    @objc dynamic var lastEventId: Int = 0
    @objc dynamic var lastEventDate: Date = Date()
    @objc dynamic var shipperName: String = ""
    @objc dynamic var shipperAddress: String = ""
    @objc dynamic var shipperId: String = ""
    @objc dynamic var totalSeconds: Int = 0
    @objc dynamic var checkoutId: String = ""
    
    override static func primaryKey() -> String? {
        return "id"
    }
    
}
